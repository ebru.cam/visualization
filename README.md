# Visualization
![pipeline](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/server/badges/master/build.svg)
![coverage](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/server/badges/master/coverage.svg)

The visualization project uses WebGL and Three.JS to illustrate the data from the simulation in a web browser.

This project communicates with the server via the _SmartFoxServer JavaScript API_, thus the data serialization and the usage of the binary WebSocket protocol for communication with the server is done via this specific API.

# Requirements

In order the project to be built locally, the following software is required:

* Apache Maven
* Java Development Kit, Version 8+
* Browser supporting WebGL
* _(optional)_ Git

To clone this github repository, one can use the following commands

          cd MontiSim
          git clone https://github.com/MontiSim/visualization.git
          
or download the project as a [.zip file](https://github.com/MontiSim/visualization/archive/master.zip)

__NOTE:__ _MontiSim_ is the directory, in which MontiSim-belonging projects should be clonned. It has to be manually created.

# Installation

Running the complete simulation platform requires deployment of the __visualization__ and __server__ repository deliverables to an installed SmartFoxServer 2X. 

## Installation of SmartFoxServer 2X

Check out [server](https://github.com/MontiSim/server#installation-of-smartfoxserver-2x) repository documentation.

## Deployment of Web application (visualization)

Depending on the used OS, in _scripts_ directory, change deployment directory in either _deploy.bat_ or _deploy.sh_ to match the directory, in which SmartFoxServer has been installed. For example:

* for linux with `<SmartFoxServer directory>` at `/opt/SmartFoxServer_2X/SFS2X`

      SFS_VIS_DIR="/opt/SmartFoxServer_2X/SFS2X/www/visualization"
    
* for windows with `<SmartFoxServer directory>` at `C:\Users\Administrator\SmartFoxServer_2X\SFS2X`

      SET SFS_VIS_DIR=C:\Users\Administrator\SmartFoxServer_2X\SFS2X\www\visualization

In _pom.xml_, the `execution` in _exec-maven-plugin_ has to be configured for phase `package` to allow automatic deployment of the web application files. `<phase>none</phase>` is used by default, to allow correct operating of contineous integration tools.

      <execution>
          <id>Deploy</id>
          <phase>none<!-- change to 'package' for production --></phase>
          <goals>
              <goal>exec</goal>
          </goals>
      </execution>
      
Deployment of the visualization as a web application is done via `mvn clean install` command, which will built, test and deploy all the files of the web application into SmartFoxServer _www_ directory. If an instance of the SmartFoxServer is running, the new web application will be acccessible by users after reload of the web page (if the user has been connected during deployment of new version).

# Running visualization

Once the SmartFoxServer is running and the _server_ and _visualization_ applications are deployed, just access the address, served by SmartFoxServer. If configuration is done according to the documentation in __server__ repository, then visualizaton should be accessible at `http://localhost/visualization`.

# Using the web application

Once connected to the server, a navigation bar will be available. It will change its content dynamically, depending on the allowed functionalities at the currently reached state.

For example, starting a visualization can be done by undertaking the following steps:
* Login
* Select scenario and a vehicle to be followed from the scenario dropdown menu
* Wait until the simulation is processed. A notification message will be displayed once the simulation is ready and will unlock the controlls.
* Click _Play_ to start visualization

For uploading a scenario:
* Login
* Click on _Upload Scenario_
* Select 2 files - a _.sim_ file describing the simulation scenario and an _.osm_ file with the OpenStreetMap map data for the scenario
* When the new scenario is uploaded on the server, the scenario dropdown menu will be updated automatically

